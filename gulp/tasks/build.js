let gulp = require('gulp'),
  runSequence  = require('run-sequence');

gulp.task('build', function() {
  console.log('BUILD JEKYLL');
  var arg = process.argv[3]
  if (arg) task = 'jekyll:draft';
  else task = 'jekyll';
  runSequence(['styles', 'img'], task);
});