// Runs Jekyll build
let gulp = require('gulp'),
  browserSync = require('browser-sync'),
  cp = require('child_process'),
  gutil = require('gulp-util'), // adesso nn lo uso e l'ho installato
  run = require('gulp-run'); // adesso non lo uso e l'ho installato

let paths = require('../paths');

let messages = {
  jekyllDev: 'Running: $ jekyll build for dev',
  jekyllProd: 'Running: $ jekyll build for prod'
};

let shellCommand = 'bundle exec jekyll build --config _config.yml';

gulp.task('jekyll', function() {
  browserSync.notify(messages.jekyllProd);
  return gulp.src(paths.jekyllDir)
    .pipe(run(shellCommand))
    .on('error', gutil.log);
});

gulp.task('jekyll:draft', function() {
  shellCommand += ' --drafts';
  browserSync.notify(messages.jekyllDev);
  return gulp.src(paths.jekyllDir)
    .pipe(run(shellCommand))
    .on('error', gutil.log);
});

/*gulp.task('jekyll-dev', function (done) {
  browserSync.notify(messages.jekyllDev);
  return cp.spawn('jekyll', ['build', '--drafts', '--config', '_config.yml'], {stdio: 'inherit'})
 .on('close', done);
});*/