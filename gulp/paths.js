var paths = {};

paths.styles        = 'temp/styles';
paths.images        = 'assets/images';
paths.appDir        = 'app/';
paths.jekyllDir     = '';
paths.siteDir       = '_site/';
paths.buildStyles   = 'css';
paths.buildImages   = 'images';

module.exports = paths;