let gulp = require('gulp');

let paths = require('../paths');

gulp.task('img', () => {
  console.log('generating IMAGE file');
  // gulp.src è una funzione asincrona
  return gulp.src('./app/assets/**/*.+(png|jpg|svg)')
    .pipe(gulp.dest(paths.jekyllDir));
});