let gulp = require('gulp'),
  browserSync = require('browser-sync').create();
  watch = require('gulp-watch');

gulp.task('watch', () => {
  browserSync.init({
    notify: true,
    //ghostMode: false, // do not mirror clicks, reloads, etc. (performance optimization)
    logFileChanges: true,
    server: {
      baseDir: '_site',
    },
    port: 4000
  });
  // Posso inserire un array di sorgenti da verificare: ['./app/index.html', './app/temp/styles/style.css']
  /*watch('./app/index.html', () => {
    browserSync.reload();
  }, ['build']);*/

  // Watch Jekyll html files
  gulp.watch(['**/*.html', '!_site/**/*.*'], ['build', () => {
    browserSync.reload();
  }]);

  watch('./app/assets/styles/**/*.+(pcss|css|postcss)', () => {
    gulp.start('cssInject');
  }, ['build']);
});

// Sfrutto la possibilità di fare l'inject di un css senza forzare il reload della pagina.
// Inoltro inserisco una dependency alla task cssInject, specificando styles che verrà effettuata prima di cssInject
gulp.task('cssInject', ['styles'],() => {
  console.log('Starting cssInject...');
  // faccio la return della funzione async gulp.src
  return gulp.src('./app/temp/styles/style.css')
    .pipe(browserSync.stream());
});