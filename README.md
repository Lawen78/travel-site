# Demo Site - Tutorial

Il presente sito utilizza un workflow definito con GULP e utilizza BEM per la definizione delle classi CSS.

BEM: blocco\_\_elemento, blocco--modificatore , blocco__elemento--modificatore


Il pattern Mobile-First seguito è tramite l'uso di POSTCSS-MIXIN:

```CSS
&__title {
  font-weight: 300;
  font-size: 1.1rem;

  @mixin atSmall{
    font-size: 2rem;
  }

  @mixin atMedium {
    font-size: 3.2rem;
  }

  @mixin atLarge {
    font-size: 4.8rem;
  }
}
```

## Approcio Mobile First

Cosa vuol dire? Quando implemento il layout del sito web, questo deve adattarsi a seconda dei devices su cui verrà visualizzato e quindi ottimizzare la dimensione dello schermo. Per questo motivo, un sito web che si adatta, è detto a layout *responsive*.

Il layout definisce la suddivisione della griglia del sito, suddividendolo in colonne in base alla larghezza dello schermo su cui è visualizzato.
Ad esempio, in un Laptop/Desktop, il layout può essere a 3 o più colonne, mentre in un Tablet potremmo presentare un layout a 2 colonne ed infine per uno smartphone un layout a singola colonna.

Il workflow consigliato, per la creazione del layout, è quello di partire definendo l'aspetto grafico più semplice e cioè quello di uno smartphone. Questo per via di alcuni fattori:

- I dispositivi mobile hanno superato i dispositivi laptop come numero di connessione ai siti web, pertanto è bene concentrarsi su un layout efficiente ed efficace per questi dispotivi;
- Per avere un sito web efficiente per i piccoli dispositivi, con connessioni a volte ridotte (e a pagamento), è bene non far scaricare nulla di grosse dimensioni o che poi verrà nascosto tramite le media query.

Rispetto ai compiti di un grafico, il Mobile First vuol dire implementare un layout che tenga conto della dimensione ridotta dello schermo, dare priorità a dei contenuti rispetto ad altri, prevedere le azioni più usate e renderle agevoli ed accessibili, migliorando il più possibile l'esperienza utente e quindi l'usabilità del sito.
Rispetto ai compiti di uno sviluppatore, il Mobile First deve occuparsi di fornire solo le risorse strettamente necessarie, senza occupare banda per risorse inutili o risorse da ridimensionare, ottimizzando i tempi di download.


## Responsive Image

Quando ho una immagine classica che devo visualizzare dal desktop al mobile, in quest'ultimo dispositivo avrei un sforzo di download non necessario (resolution), inoltre avrei una immagine troppo piccola rispetto allo schermo e magari la vorrei ritagliata (crop) rispetto ad un soggetto. La soluzione è inviare differenti immagini per differenti dispositivi. Questa è una immagine responsive.
Preparo 3 immagini: small - medium - large con diversi crop, oppure preparo 3 immagini identiche con differenti dimensioni (e quindi risoluzioni).

Anzichè utilizzare l'elemento img, andiamo a wrapparlo all'interno di un elemento picture in cui inserisco l'immagine "base" (quella più piccola) per il mobile e due source con una media query per il medium e la large:

```
<picture>
  <source srcset="img/dog-crop-large.jpg" media="(min-width: 1200px)">
  <source srcset="img/dog-crop-medium.jpg" media="(min-width: 760px)">
  <img src="img/dog-crop-small.jpg" alt="Puppy at the beach">
</picture>
```

Se invece voglio presentare diverse risoluzioni di immagine non uso il picture element:

```
<img srcset="img/dog-resolution-small.jpg 570w, img/dog-resolution-media.jpg 1200w, img/dog-resolution-large.jpg 1920w" alt="Puppy at beach">
```

Non ho specificato nessuna media query. Come fa il browser a caricare la corretta immagine? Il browser conosce la dimensione del proprio schermo e pertanto userà l'immagine più appropriata. Se parto con una dimensione larga dello schermo e ridimensionassi la finestra del browser, non avrò il download ulteriore dell'immagine media e piccola ma verrà ridimensionata l'immagine larga.

Posso avere un mix di queste due tecniche per inviare file di dimensioni o ritagli differenti a differenti densità di pixel:

```
<picture>
  <source srcset="assets/images/hero--large.jpg 1920w, assets/images/hero--large-hi-dpi.jpg 3840w" media="(min-width: 1380px)">
  <source srcset="assets/images/hero--medium.jpg 1380w, assets/images/hero--medium-hi-dpi.jpg 2760w" media="(min-width: 990px)">
  <source srcset="assets/images/hero--small.jpg 990w, assets/images/hero--small-hi-dpi.jpg 1980w" media="(min-width: 640px)">
  <img srcset="assets/images/hero--smaller.jpg 640w, assets/images/hero--smaller-hi-dpi.jpg 1280w" alt="Coastal view of ocean and moutains">
</picture>
```

## Jekyll

creo il file _config.yml con :

```
exclude:
  - package.json
  - node_modules
  - gulpfile.js
```

Ho installato child_process, gulp-util, run-sequence, gulp-replace-path, del e gulp-run

guida che sto [seguendo](https://robwise.github.io/blog/jekyll-and-gulp)

[github](https://github.com/robwise/robwise.github.io)

[altro sito](https://www.chenhuijing.com/blog/gulp-jekyll-github/#🎹)

Se faccio jekyll serve, viene rigenerato la folder _site, ma siccome ho fatto il clean dei file html della root, non mi viene generato nessun index!
Inoltre non ha senso avere i file html in app? Meglio spostarli direttamente in root?
La watch non ha più senso intesa come prima, ma deve essere una watch del jekyll.

Ho tolto la task HTML e tolto il clean in Build.

Per avviare il server: gulp watch :)

DEVO FARE PULIZIA NEL CODICE :)

