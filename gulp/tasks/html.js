let gulp = require('gulp'),
  replace = require('gulp-replace-path');

let paths = require('../paths');

gulp.task('html', () => {
  console.log('generating HTML file');
  // gulp.src è una funzione asincrona
  return gulp.src('./app/**/*.+(html|htm)')
    .pipe(replace(paths.styles, paths.buildStyles))
    .pipe(replace(paths.images, paths.buildImages))
    .pipe(gulp.dest(paths.jekyllDir));
});