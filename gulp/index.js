let styles = require('./tasks/styles'),
  watch = require('./tasks/watch'),
  jekyll = require('./tasks/jekyll'),
  build = require('./tasks/build'),
  img = require('./tasks/img');

module.exports = {
  styles,
  watch,
  jekyll,
  build,
  img
};