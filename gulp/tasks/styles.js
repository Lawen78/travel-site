let gulp = require('gulp'),
	postcss = require('gulp-postcss'),
	autoprefixer = require('autoprefixer'),
	cssVars = require('postcss-simple-vars'),
	nested = require('postcss-nested'),
	cssImport = require('postcss-import'),
	mixins = require('postcss-mixins');

let paths = require('../paths');

gulp.task('styles', () => {
  console.log('some stuff being done to my CSS/SASS or PostCSS file');
  // gulp.src è una funzione asincrona
  return gulp.src('./app/assets/styles/style.css')
    .pipe(postcss([ cssImport, mixins, cssVars, nested, autoprefixer() ]))
		// Quando utilizzo il this attenzione ad usare l'arrow function
    .on('error', function(error) {
			console.log(error.toString());
			// in caso di errore, emetto l'evento end per GULP
			this.emit('end');
    })
		.pipe(gulp.dest(paths.jekyllDir + 'css'))
    .pipe(gulp.dest(paths.siteDir + 'css'))
    .pipe(gulp.dest('./app/temp/styles'));
});